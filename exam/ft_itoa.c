/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkaliber <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/23 15:43:24 by mkaliber          #+#    #+#             */
/*   Updated: 2017/01/23 17:42:54 by mkaliber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>

int		ft_len(int nb, int base)
{

	int i;

	i  = 0;
	if (nb < 0 && base == 10)
	{
		i++;
		nb = -nb;
	}
	else if (nb < 0)
	{
		nb = -n
	}
	while (nb > 0)
	{
	//	printf("nb %d\n", nb);
		nb = nb / base;
		i++;
	}
	//printf("!!!!!!!!!!!!i%d \n", i);
	return (i);
}

char	*ft_anton(char *str, int nb, int len, int base)
{
	int i;
	
	i = 0;
	if (nb < 0 && base == 10)
	{
		str[i] = '-';
		i++;
		nb = -nb;
	}
	else if(nb < 0)
		nb = -nb;
	str[len] = '\0';
	while(len > i)
	{
		printf("while   %d \n", nb);
		if (nb % base <= 9)
		{
			str[len - 1] = nb % base  + 48;
		}
		if(nb % base > 9)
		{
			str[len - 1] = nb % base  + 55;	
			printf("zashlo posle 9  %d \n", nb);
		}
		nb = nb / base;
		len--;
	}
	return (str);
}

char	*ft_itoa(int nb, int base)
{
	int len;

	char *str;
	len = ft_len(nb, base);
	if (!(str = (char *)malloc(len * sizeof(*str))))
		return (0);
	return(ft_anton(str, nb, len, base));
}

int main()
{
	printf("%s", ft_itoa(255, 16));
	return (0);
}
