/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_foreach.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkaliber <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/26 18:32:46 by mkaliber          #+#    #+#             */
/*   Updated: 2016/12/26 18:36:52 by mkaliber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


typedef	stptruct	s_list
{
	struct s_list *next;
	void          *data;
}                 	t_list;

void    ft_list_foreach(t_list *begin_list, void (*f)(void *))
{
	while (begin_list)
	{
		f(begin_list);
		lst = lst->next;
	}
}
