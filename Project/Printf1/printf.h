/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printf.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkaliber <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/07 14:48:52 by mkaliber          #+#    #+#             */
/*   Updated: 2017/02/09 15:26:20 by mkaliber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PRINTF_H
# define PRINTF_H
# include <unistd.h>
# include <stdio.h>
# include <stdarg.h>
# include <string.h>
# include <inttypes.h>
# include "libft/libft.h"
# include <wchar.h>
# include <stdint.h>

struct home {

    int     g;
    int     gsp;
    int     iter;
    int     minus;
    int     plus;
    int     null;
    int     sharp;
    char    *str_text_f;
    char    *flag_list;
    char    *str_text_s;
    char    *str_gen;
    char    *str_flag;
}  home1;

	char *wd;
    int ind;
    int glen;
    int gil;
    int gi;
    int gw;
    int gfst;
    int gsnd;
    int gst;
    int gwd;

int     ft_printf(char *format, ...);
char	*ft_itoa_base(intmax_t nb, long long int base);
char	*ft_itoa_base_p(unsigned long long int nb, int base);
int     check(char * str);
void    *check_text_f(char *str);
void    *check_text_s(char *str);
void    *check_text_f(char *str);
void    check_space();
char	check_flag(char * str);
void    iter_flag();

void    flag_list(char *form);

void    check_plus();
void    check_accuracy();
void    check_width_wdef();
void    check_width_def();
int     check_acc();
void    ft_gen_minus();
int     save_n_acc();
int     save_n_width();
char    inc_null_d();
int     save_n_acc_gen();
int     save_n_width_gen();
void    space();
void    plus();

void    *typer_di(va_list ap);
int		check_di(char *str, int tmp);
int		check_di_hh_ll(char *str, int tmp);
int		check_di_j(char *str, int tmp);
int		check_di_z(char *str, int tmp);
int		check_o(char *str, int tmp, int i);
int		check_oO_hh_ll(char *str, int tmp);
int		check_oO_j(char *str, int tmp);
int		check_oO_z(char *str, int tmp);

int		check_u(char *str, int tmp, int i);
int		check_uU_hh_ll(char *str, int tmp);
int		check_uU_j(char *str, int tmp);
int		check_uU_z(char *str, int tmp);

int		check_x(char *str, int tmp, int i);
int		check_xX_hh_ll(char *str, int tmp);
int		check_xX_j(char *str, int tmp);
int		check_xX_z(char *str, int tmp);

char    *ft_checktype(int i);
void    *typer_s(va_list ap);
void    *typer_S_l(va_list ap);
void    *typer_c(va_list ap);
void    *typer_C_l(va_list ap);
void    *typer_s(va_list ap);
void    *typer_p(va_list ap);
void    *typer_di(va_list ap);
void    *typer_di_h(va_list ap);
void    *typer_di_hh(va_list ap);
void    *typer_di_l(va_list ap);
void    *typer_di_ll(va_list ap);
void    *typer_di_j(va_list ap);
void    *typer_di_z(va_list ap);

void    *typer_o(va_list ap);
void    *typer_O(va_list ap);
void    *typer_oO_h(va_list ap);
void    *typer_oO_hh(va_list ap);
void    *typer_oO_l(va_list ap);
void    *typer_oO_ll(va_list ap);
void    *typer_oO_j(va_list ap);
void    *typer_oO_z(va_list ap);

void    *typer_u(va_list ap);
void    *typer_U(va_list ap);
void    *typer_uU_h(va_list ap);
void    *typer_uU_hh(va_list ap);
void    *typer_uU_l(va_list ap);
void    *typer_uU_ll(va_list ap);
void    *typer_uU_j(va_list ap);
void    *typer_uU_z(va_list ap);

void    *typer_x(va_list ap);
void    *typer_X(va_list ap);
void    *typer_xX_h(va_list ap);
void    *typer_xX_hh(va_list ap);
void    *typer_xX_l(va_list ap);
void    *typer_xX_ll(va_list ap);
void    *typer_xX_j(va_list ap);
void    *typer_xX_z(va_list ap);

int		check_modef(char *f);
char		check_fl(char str);
void    flag_list_l(char *f, int start);
void    fl_l(char *f, size_t n);
char		check_inst(char str);
void    sharp();
char		check_fll(char *str, int start);
char		check_instl(char str);

#endif
