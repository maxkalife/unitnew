/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   typers_uU_hh_ll.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkaliber <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/09 15:07:20 by mkaliber          #+#    #+#             */
/*   Updated: 2017/02/09 16:23:04 by mkaliber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "printf.h"


void    *typer_uU_h(va_list ap)
{
    char *p;
	intmax_t i;

	i = va_arg(ap, unsigned long long);
	if (i == 4294967296)
		return ("4294967296");
	p = ft_itoa_base(i, 10);
    return (p);
}

void    *typer_uU_hh(va_list ap)
{
    char *p;

    p = ft_itoa_base((unsigned char)va_arg(ap, unsigned long long), 10);
    return (p);
}

void    *typer_uU_l(va_list ap)
{
    char *p;

    p = ft_itoa_base(va_arg(ap, unsigned long int), 10);
    return (p);
}

void    *typer_uU_ll(va_list ap)
{
    char *p;

    p = ft_itoa_base(va_arg(ap, unsigned long long int), 10);
    return (p);
}