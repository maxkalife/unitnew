/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_sc_l.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkaliber <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/09 10:12:13 by mkaliber          #+#    #+#             */
/*   Updated: 2017/02/09 15:18:39 by mkaliber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "printf.h"

int     check_s_S_l(char *str, int tmp);
int     check_c_C_l(char *str, int tmp);

int		check(char *str)
{
	int tmp;

	ind = 0;
	tmp = ind;
	while (str[ind])
	{
		if (str[ind] == 's')
			return (check_s_S_l(str, tmp));
		if (str[ind] == 'c')
			return (check_c_C_l(str, tmp));
		if (str[ind] == 'p')
			return (4);
		if (str[ind] == 'd' || str[ind] == 'i')
			return (check_di(str, tmp));
		if (str[ind] == 'o' || str[ind] == 'O')
			return (check_o(str, tmp, ind));
		if (str[ind] == 'u' || str[ind] == 'U')
			return (check_u(str, tmp, ind));
		if (str[ind] == 'x' || str[ind] == 'X')
			return (check_x(str, tmp, ind));
		ind++;
	}
	return (365);
}

int     check_s_S_l(char *str, int tmp)
{
	while (str[tmp])
	{
		if (str[tmp] == 'l')
		{
			return (1);
		}
		tmp++;
	}
	return (0);
}

int     check_c_C_l(char *str, int tmp)
{
	while (str[tmp])
	{
		if (str[tmp] == 'l')
		{
			return (3);
		}
		tmp++;
	}
	return (2);
}

int     check_di(char *str, int tmp)
{
	if (check_di_hh_ll(str, tmp) != 5)
		return (check_di_hh_ll(str, tmp));
	else if (check_di_j(str, tmp) != 5)
		return (check_di_j(str, tmp));
	else if (check_di_z(str, tmp) != 5)
		return (check_di_z(str, tmp));
	tmp++;
	return (5);
}

int     check_o(char *str, int tmp, int i)
{
	int o;

	if (str[i] == 'o')
		o = 12;
	else
		o = 13;
	if (check_oO_hh_ll(str, tmp) != 1)
		return (check_oO_hh_ll(str, tmp));
	if (check_oO_j(str, tmp) != 1)
		return (check_oO_j(str, tmp));
	if (check_oO_z(str, tmp) != 1)
		return (check_oO_z(str, tmp));
	return (o);
}

int     check_u(char *str, int tmp, int i)
{
	int o;

	if (str[i] == 'u')
		o = 20;
	else
		o = 21;
	if (check_uU_hh_ll(str, tmp) != 1)
		return (check_uU_hh_ll(str, tmp));
	if (check_oO_j(str, tmp) != 1)
		return (check_uU_j(str, tmp));
	if (check_oO_z(str, tmp) != 1)
		return (check_uU_z(str, tmp));
	return (o);
}

int		check_x(char *str, int tmp, int i)
{
	int o;

	if (str[i] == 'x')
		o = 28;
	else
		o = 29;
	if (check_xX_hh_ll(str, tmp) != 1)
		return (check_xX_hh_ll(str, tmp));
	if (check_oO_j(str, tmp) != 1)
		return (check_xX_j(str, tmp));
	if (check_oO_z(str, tmp) != 1)
		return (check_xX_z(str, tmp));
	return (o);
}
