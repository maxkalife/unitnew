/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   typers_di_hh_ll.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkaliber <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/09 15:07:20 by mkaliber          #+#    #+#             */
/*   Updated: 2017/02/09 16:23:04 by mkaliber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "printf.h"


void    *typer_di(va_list ap)
{
	char *p;
	intmax_t i;

	i = va_arg(ap, int);
	p = ft_itoa_base(i, 10);
	return (p);
}

void    *typer_di_h(va_list ap)
{
	char *p;

	p = ft_itoa_base((short int)va_arg(ap, int), 10);
	return (p);
}

void    *typer_di_hh(va_list ap)
{
	char *p;

	p = ft_itoa_base((signed char)va_arg(ap, int), 10);
	return (p);
}

void    *typer_di_l(va_list ap)
{
	char *p;

	p = ft_itoa_base(va_arg(ap, long int), 10);
	return (p);
}

void    *typer_di_ll(va_list ap)
{
	char *p;
	intmax_t i;

	i = va_arg(ap, intmax_t);
	if (i < -9223372036854775807)
		return ("-9223372036854775808");
	p = ft_itoa_base(i, 10);
	return (p);
}

