/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_base_p.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkaliber <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/23 15:43:24 by mkaliber          #+#    #+#             */
/*   Updated: 2017/01/23 17:42:54 by mkaliber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "printf.h"

int		ft_len_p(unsigned long long int nb, int base)
{
	unsigned long long int i;

	i  = 0;
	while (nb > 0)
	{
		nb = nb / base;
		i++;
	}
	return (i);
}

char	*ft_anton_p(char *str, unsigned long long int nb, unsigned long long int len, int base)
{
	unsigned long long int i;

	i = 0;
	str[len] = '\0';
	while(len > i)
	{
		if (nb % base <= 9)
		{
			str[len - 1] = nb % base  + 48;
		}
		if(nb % base > 9)
		{
			str[len - 1] = nb % base  + 55;
		}
		nb = nb / base;
		len--;
	}
	return (ft_strlower(str));
}

char	*ft_itoa_base_p(unsigned long long int nb, int base)
{
	unsigned long long int len;

	char *str;
	len = ft_len_p(nb, base);
	if (!(str = (char *) malloc(len * sizeof(*str))))
		return (0);
	return (ft_anton_p(str, nb, len, base));
}
