/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_di_fl.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkaliber <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/09 10:12:13 by mkaliber          #+#    #+#             */
/*   Updated: 2017/02/09 15:18:39 by mkaliber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "printf.h"

int		check_di_hh_ll(char *str, int tmp)
{
	int h;
    int l;

	h = 0;
	l = 0;
	while (str[tmp] == 'h')
	{
		tmp++;
		h++;
	}
	if (h == 1)
		return (6);
	if (h == 2)
		return (7);
	while (str[tmp] == 'l')
	{
		tmp++;
		l++;
	}
	if (l == 1)
		return (8);
	if (l == 2)
		return (9);
	return (5);
}

int		check_di_j(char *str, int tmp)
{
	while (str[tmp])
	{
		if (str[tmp] == 'j')
		{
			return (10);
		}
		tmp++;
	}
	return (5);
}

int		check_di_z(char *str, int tmp)
{
	while (str[tmp])
	{
		if (str[tmp] == 'z')
		{
			return (11);
		}
		tmp++;
	}
	return (5);
}

int		check_di_t(char *str, int tmp)
{
	while (str[tmp])
	{
		if (str[tmp] == 't')
		{
			return (12);
		}
		tmp++;
	}
	return (5);
}