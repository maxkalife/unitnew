/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   flag_list.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkaliber <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/29 16:06:42 by mkaliber          #+#    #+#             */
/*   Updated: 2017/03/29 16:06:44 by mkaliber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "printf.h"

void    flag_list_l(char *f, int start)
{
	int n;
	int i;
	int r;

	n = 1;
	i = start;
	while (f[i])
	{
		gst = gsnd;
		r = gfst;
		if (f[i] == '%' && f[i + 1] != '%' && check_fll(f, i + 1) == 'a')
		{
			i++;
			gfst = i;
			while (f[i] != '%' && f[i] && check_inst(f[i]) != 'z')
			{
				n++;
				i++;
			}
			gsnd = i + 1;
			fl_l(f, n);
			break ;
		}
		i++;
	}
	//printf("gst------ %d\n", gst);
	//printf("gfst------ %d\n", gfst);
	//printf("gsnd------ %d\n", gsnd);

}

void    fl_l(char *f, size_t n)
{
	int i;
	int g;

	g = 0;
	i = gfst;
	home1.flag_list = ft_strnew(n);
	while (i != gsnd)
	{
		home1.flag_list[g] = f[i];
		i++;
		g++;
	}
}

char		check_fl(char str)
{
	char    res;

	res = 'z';
	if (str == 's')
		return ('a');
	if (str == 'c')
		return ('a');
	if (str == 'p')
		return ('a');
	if (str == 'd' || str == 'i')
		return ('a');
	if (str == 'o' || str == 'O' || str == 'z')
		return ('a');
	if (str == 'u' || str == 'U' || str == 'x' || str == 'X')
		return ('a');
	return (res);
}

char		check_fll(char *str, int i)
{
	char    res;

	res = 'z';
	while (str[i] && check_instl(str[i]) != 'z')
	{
		if (check_fl(str[i]) == 'a')
			return ('a');
		i++;

	}
	return (res);
}

char		check_inst(char str)
{
	char    res;

	res = 'z';
	if (str == '-' || str == '+' || (str >= '0' && str <= '9') || str == '#' || str == 'z')
		return ('u');
	if (str == 'h' || str == 'l' || str == 'j' || str == 't' || str == '.' || str == ' ')
		return ('u');
	return (res);
}

char		check_instl(char str)
{
	char    res;

	res = 'z';
	if (str == '-' || str == '+' || (str >= '0' && str <= '9') || str == '#' || str == 'z')
		return ('u');
	if (str == 'h' || str == 'l' || str == 'j' || str == 't' || str == '.' || str == ' ')
		return ('u');
	if (str == 's')
		return ('a');
	if (str == 'c')
		return ('a');
	if (str == 'p')
		return ('a');
	if (str == 'd' || str == 'i')
		return ('a');
	if (str == 'o' || str == 'O')
		return ('a');
	if (str == 'u' || str == 'U' || str == 'x' || str == 'X')
		return ('a');
	return (res);
}