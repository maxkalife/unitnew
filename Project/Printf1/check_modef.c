/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_modef.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkaliber <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/29 12:43:07 by mkaliber          #+#    #+#             */
/*   Updated: 2017/03/29 12:43:12 by mkaliber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "printf.h"

int		check_modef(char *f)
{
	int i;
	int n;
	int g;

	g = 0;
	i = 0;
	n = 0;
	while (f[i])
	{
	//	printf("fl --------- %c\n", check_fll(f, i));
	//	printf("n --------- %c\n", f[i]);
		if (f[i] == '%' && f[i + 1] != '%' && check_fll(f, i + 1) == 'a')
		{
			i++;
			while (f[i] != '%' && f[i] && check_inst(f[i]) != 'z')
			{
				n++;
				i++;
			}
			if (check_fl(f[i]) != 'z')
				n++;
		}
		if (n > 0)
		{
			g++;
			n = 0;
		}
		i++;
	}
	//printf("n --------- %d\n", g);
	return (g);
}