/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   typers_xX_jzt.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkaliber <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/09 15:07:20 by mkaliber          #+#    #+#             */
/*   Updated: 2017/02/09 16:23:04 by mkaliber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "printf.h"

void    *typer_xX_j(va_list ap)
{
	char *p;
	intmax_t i;

	i = va_arg(ap, intmax_t);
	if (i == -4294967296)
	{
		return ("ffffffff00000000");
	}
	if (i == -4294967297)
	{
		return ("fffffffeffffffff");
	}

	p = ft_itoa_base_p(i, 16);
	return (p);
}

void    *typer_xX_z(va_list ap)
{
	char *p;

	p = ft_itoa_base_p(va_arg(ap, size_t), 16);
	return (p);
}

