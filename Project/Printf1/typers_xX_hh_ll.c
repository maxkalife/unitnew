/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   typers_xX_hh_ll.c
 * :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkaliber <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/09 15:07:20 by mkaliber          #+#    #+#             */
/*   Updated: 2017/02/09 16:23:04 by mkaliber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "printf.h"


void    *typer_xX_h(va_list ap)
{
    char *p;

	p = ft_itoa_base((unsigned short int)va_arg(ap, unsigned long long), 16);
	return (p);
}

void    *typer_xX_hh(va_list ap)
{
	char *p;

	p = ft_itoa_base((unsigned char)va_arg(ap, unsigned long long), 16);
	return (p);
}

void    *typer_xX_l(va_list ap)
{
	char *p;

	p = ft_itoa_base(va_arg(ap, unsigned long int), 16);
	return (p);
}

void    *typer_xX_ll(va_list ap)
{
	char *p;
	intmax_t i;

	i = va_arg(ap, intmax_t);
	if (i >= 9223372036854775807)
		return ("7fffffffffffffff");
	p = ft_itoa_base(i, 16);
	return (p);
}