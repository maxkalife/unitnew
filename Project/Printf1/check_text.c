/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_text.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkaliber <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/14 16:30:50 by mkaliber          #+#    #+#             */
/*   Updated: 2017/03/29 10:02:12 by mkaliber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "printf.h"
int     width(char *str, char null, int g, int min);

void    *check_text_f(char *str)
{
	int     n;
	int     g;
	char    *s;
	char    null;
	int     min;

	n = gst;
	g = gst;
	while (n != gfst - 1)
		n++;
	s = ft_strnew(n + 1);
	n = 0;
	null = ' ';
	wd = ft_strnew(0);
	while (g != gfst - 1)
	{
		if (str[g] == '%')
		{
			g++;
			while (str[g] == ' ' || str[g] == '.')
				g++;
			if (str[g] == '0')
			{
				g++;
				null = '0';
			}
			if (str[g] == '-')
			{
				g++;
				min = 1;
			}
			if (str[g] >= '0' && str[g] <= '9')
				g = width(str, null, g, min);
		}
		if (g != gfst - 1)
		{
			s[n] = str[g];
			n++;
			g++;
		}
	}
	s = ft_strjoin(wd, s);
	return (s);
}

int     width(char *str, char null, int g, int min)
{
	int     w;
	int     n;

	n = 0;
	w = 0;
	if (str[g] == '0')
		null = 0;
	w = w + (str[g] - '0');
	g++;
	while (str[g] >= '0' && str[g] <= '9' && str[g])
	{
		w = w * 10 + (str[g] - '0');
		g++;
	}
	wd = ft_strnew(w - 1);
	if (min == 1)
	{
		wd[n] = str[g];
		g++;
		n++;
	}
	while (w > 1)
	{
		wd[n] = null;
		w--;
		n++;
	}
	wd[n] = '\0';
	return (g);
}

void    *check_text_s(char *str)
{
	int     n;
	int     g;
	char    *s;

	n = gsnd;
	g = gsnd;
	while (str[n])
		n++;
	s = ft_strnew(n);
	n = 0;
	while (str[g])
	{
		s[n] = str[g];
		n++;
		g++;
	}
	return (s);
}