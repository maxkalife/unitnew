/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   typers_uU_jzt.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkaliber <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/09 15:07:20 by mkaliber          #+#    #+#             */
/*   Updated: 2017/02/09 16:23:04 by mkaliber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "printf.h"

void    *typer_uU_j(va_list ap)
{
	char *p;

	p = ft_itoa_base(va_arg(ap, uintmax_t), 10);
	return (p);
}

void    *typer_uU_z(va_list ap)
{
	char *p;

	p = ft_itoa_base(va_arg(ap, size_t), 10);
	return (p);
}

