/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   typers_oOuU.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkaliber <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/09 15:07:20 by mkaliber          #+#    #+#             */
/*   Updated: 2017/02/09 16:23:04 by mkaliber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "printf.h"


void    *typer_o(va_list ap)
{
	char *p;

	p = ft_itoa_base_p(va_arg(ap, unsigned int), 8);
	return (p);
}

void    *typer_O(va_list ap)
{
	char *p;

	p = ft_itoa_base_p(va_arg(ap, unsigned long int), 8);
	return (p);
}

void    *typer_u(va_list ap)
{
	char *p;

	p = ft_itoa_base_p(va_arg(ap, unsigned int), 10);
	return (p);
}

void    *typer_U(va_list ap)
{
	char *p;

	p = ft_itoa_base_p(va_arg(ap, unsigned long int), 10);
	return (p);
}

