//
// Created by Maksym Kaliberda on 3/24/17.
//

#include "printf.h"


int     main()
{
    int r;
    int o;

    r = ft_printf("@moulitest: %.o", 0);
	o = printf("@moulitest: %o", 0);
    printf("\nr ------------ %d", r);
	printf("\n0 ------------ %d", o);
    return (0);
}