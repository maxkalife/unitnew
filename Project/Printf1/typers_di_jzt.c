/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   typers_di_jzt.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkaliber <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/09 15:07:20 by mkaliber          #+#    #+#             */
/*   Updated: 2017/02/09 16:23:04 by mkaliber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "printf.h"

void    *typer_di_j(va_list ap)
{
	char *p;

	p = ft_itoa_base(va_arg(ap, intmax_t), 10);
	return (p);
}

void    *typer_di_z(va_list ap)
{
	char *p;

	p = ft_itoa_base(va_arg(ap, size_t), 10);
	return (p);
}
