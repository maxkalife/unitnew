/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_flag.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkaliber <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/16 13:02:42 by mkaliber          #+#    #+#             */
/*   Updated: 2017/03/29 10:00:09 by mkaliber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "printf.h"


char		check_flag(char *str)
{
	int     i;
	char    res;
	
	i = 0;
	res = 'z';
	while (str[i])
	{
		if (str[i] == 's')
			return ('s');
		if (str[i] == 'c')
			return ('c');
		if (str[i] == 'p')
			return ('p');
		if (str[i] == 'd' || str[i] == 'i')
			return ('d');
		if (str[i] == 'o' || str[i] == 'O')
			return ('o');
		if (str[i] == 'u' || str[i] == 'U')
			return ('u');
		if (str[i] == 'x')
			return ('x');
		if (str[i] == 'X')
			return ('X');
		i++;
	}
	return (res);
}
