/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   typers_p.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkaliber <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/09 15:07:20 by mkaliber          #+#    #+#             */
/*   Updated: 2017/02/09 16:23:04 by mkaliber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "printf.h"

void	*typer_p(va_list ap)
{
	char *p;
	char *r;
	int i;

	i = 0;
	char x[2] = "0x";

	if (!(p = (char *) malloc(3 * sizeof(p))))
		return (0);
	while (i < 3)
	{
		p[i] = x[i];
		i++;
	}
	p[3] = '\0';
	r = ft_itoa_base_p((unsigned long long int)(va_arg(ap, void *)), 16);
	p = ft_strjoin(p, r);
	return (p);
}