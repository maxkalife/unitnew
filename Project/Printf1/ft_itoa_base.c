/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkaliber <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/23 15:43:24 by mkaliber          #+#    #+#             */
/*   Updated: 2017/01/23 17:42:54 by mkaliber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "printf.h"

int     ft_len(intmax_t nb, int base)
{
	intmax_t i;

	i  = 0;

	if (nb < 0 && base == 10)
	{
		i++;
		nb = -nb;
	}
	if (nb < 0)
	{
		nb = -nb;
	}
	while (nb > 0)
	{
		nb = nb / base;
		i++;
	}
	return (i);
}

char	*ft_anton(char *str, intmax_t nb, intmax_t len, long long int base)
{
	long long int i;

	i = 0;
	if (nb < 0 && base == 10)
	{
		str[i] = '-';
		i++;
		nb = -nb;
	}
	else if(nb < 0)
		nb = -nb;
	str[len] = '\0';
	while(len > i)
	{
		if (nb % base <= 9)
		{
			str[len - 1] = nb % base  + 48;
		}
		if(nb % base > 9)
		{
			str[len - 1] = nb % base  + 55;
		}
		nb = nb / base;
		len--;
	}
	return (str);
}

char	*ft_itoa_base(intmax_t nb, long long int base)
{
	intmax_t len;
	char *str;


	len = ft_len(nb, base);
	if (!(str = (char *)malloc(len * sizeof(*str))))
		return (0);
	return(ft_anton(str, nb, len, base));
}