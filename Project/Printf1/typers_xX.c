/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   typers_xX.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkaliber <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/09 15:07:20 by mkaliber          #+#    #+#             */
/*   Updated: 2017/02/09 16:23:04 by mkaliber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "printf.h"

void    *typer_x(va_list ap)
{
	char *p;

	p = ft_itoa_base(va_arg(ap, unsigned int), 16);
	return (ft_strlower(p));
}

void    *typer_X(va_list ap)
{
	char *p;

	p = ft_itoa_base(va_arg(ap, unsigned int), 16);
	return (ft_strupper(p));
}