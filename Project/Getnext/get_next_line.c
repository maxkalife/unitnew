/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkaliber <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/26 10:14:48 by mkaliber          #+#    #+#             */
/*   Updated: 2017/02/02 12:32:11 by mkaliber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

char	*ft_rfile(int fd, char *st)
{
	char s[BUFF_SIZE + 1];
	int r;

	if (!(st = (char *)malloc((1) * sizeof(st))))
		return (0);
	st[0] = '\0';
	while((r = read(fd, s, BUFF_SIZE)))
	{
		s[r] = '\0';
		st = ft_strjoin(st, s);
	}
	return (st);
}

void	write_str(int fd, char *str, char *r, char **st)
{
	int i;
	int n;

	i = 0;
	n = 0;
	while (st[fd][i] != '\0' && st[fd][i] != '\n')
	{
		str[i] = st[fd][i];
		i++;
	}
	while (st[fd][i++] != '\0')
	{
		r[n] = st[fd][i];
		n++;
	}
	r[n] = '\0';
}

char	*get_next(int fd)
{
	char *r;
	char *str;
	static char *st[4000];
	
	if (st[fd] == NULL)
		st[fd] = ft_rfile(fd, st[fd]);
	r = ft_strnew(ft_strlen(st[fd]));
	str = ft_strnew(ft_strlen(st[fd]));
	if (st[fd][0] == '\n')
		str[0] = '\n';
	write_str(fd, str, r, st);
	st[fd] = r;
	return (str);
}

int		get_next_line(int const fd, char **line)
{
	char *r;
	int res;

	res = 1;	
	if (!line || fd < 0 || BUFF_SIZE < 1)
		return (-1);
	if ((read(fd, 0, 0)))
		return (-1);
	if (!(r = (char *)malloc((1) * sizeof(r))))
		return (0);
	r[0] = '\0';
	r = get_next(fd);
	if (r[0] == '\0')
		return (0);
	if (!(*line = (char *)malloc((1) * sizeof(*line))))
		*line[0] = '\0';
	if (r[0] != '\n')
		*line = ft_strsub(r, 0, ft_strlen(r));
	return(1);
}
