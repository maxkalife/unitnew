/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkaliber <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/01 13:19:43 by mkaliber          #+#    #+#             */
/*   Updated: 2017/02/02 12:35:33 by mkaliber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

int		main(int argc, char **argv)
{
	int fd;
	int fd1;
	int fd2;
	char *line;

	line = NULL;
	argc = 1;
	fd = open(argv[1], O_RDONLY);
	fd1 = open(argv[2], O_RDONLY);
	fd2 = open(argv[3], O_RDONLY);
	printf("main %d \n", get_next_line(1000000, &line));
	printf("main %d \n", get_next_line(fd1, &line));
	printf("main %d \n", get_next_line(fd, &line));
	printf("main %d \n", get_next_line(fd2, &line));
	printf("main %d \n", get_next_line(fd, &line));
	printf("main %d \n", get_next_line(fd2, &line));
	return (0);
}
