/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_database.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtelega <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/19 13:46:12 by dtelega           #+#    #+#             */
/*   Updated: 2017/01/06 14:15:47 by dtelega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

char	*ft_database(int i)
{
	char	**database;

	if (!(database = (char **)malloc(20 * sizeof(**database))))
		return (0);
//	database[i] = (char *)malloc(8 * sizeof(*database));
	database[0] = (char *)malloc(8 * sizeof(*database));
	database[0] = "####\0\0\0\0"; // A
	database[1] = (char *)malloc(8 * sizeof(*database));
	database[1] = "#0#0#0#\0"; // B
	database[2] = (char *)malloc(8 * sizeof(*database));
	database[2] = "#0#0##\0\0"; // C
	database[3] = (char *)malloc(8 * sizeof(*database));
	database[3] = "#0#1##\0\0"; // D
	database[4] = (char *)malloc(8 * sizeof(*database));
	database[4] = "#2###\0\0\0"; // E
	database[5] = (char *)malloc(8 * sizeof(*database));
	database[5] = "#0###\0\0\0"; // F
	database[6] = (char *)malloc(8 * sizeof(*database));
	database[6] = "##0#0#\0\0"; // G
	database[7] = (char *)malloc(8 * sizeof(*database));
	database[7] = "##1#0#\0\0"; // H
	database[8] = (char *)malloc(8 * sizeof(*database));
	database[8] = "###2#\0\0\0"; // I 
	database[9] = (char *)malloc(8 * sizeof(*database));
	database[9] = "###0#\0\0\0"; // J   
	database[10] = (char *)malloc(8 * sizeof(*database));
	database[10] = "##1##\0\0\0"; // K
	database[11] = (char *)malloc(8 * sizeof(*database));
	database[11] = "#1###\0\0\0"; // L
	database[12] = (char *)malloc(8 * sizeof(*database));
	database[12] = "#0##1#\0\0"; // M
	database[13] = (char *)malloc(8 * sizeof(*database));
	database[13] = "#1##0#\0\0"; // N
	database[14] = (char *)malloc(8 * sizeof(*database));
	database[14] = "###1#\0\0\0"; // O
	database[15] = (char *)malloc(8 * sizeof(*database));
	database[15] = "##2##\0\0\0"; // P
	database[16] = (char *)malloc(8 * sizeof(*database));
	database[16] = "#0##0#\0\0"; // Q
	database[17] = (char *)malloc(8 * sizeof(*database));
	database[17] = "##0##\0\0\0"; // R
	database[18] = (char *)malloc(8 * sizeof(*database));
	database[18] = "#1##1#\0\0\0"; // S
	database[19] = NULL;
	return (database[i]);
}
