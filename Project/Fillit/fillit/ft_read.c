/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_read.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtelega <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/16 14:32:21 by dtelega           #+#    #+#             */
/*   Updated: 2017/01/07 14:24:06 by dtelega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"
#include <stdio.h>

char	*ft_read(char *field, int i, int k)
{
	int		fd;
	char	c;
	char	*str;
	char	*fig;

	if (!(fd = open(field, O_RDONLY)) ||
		!(str = (char *)malloc(21 * sizeof(*str))) ||
		!(fig = (char *)malloc(27 * sizeof(*fig))))
		return (0);
	while (read(fd, &c, 1))
	{
		str[i++] = c;
		if (i == 20 && (str[19] == '\n' && (c == '\n' || c == '\0')))
		{
			str[20] = '\0';
			if (ft_check_fail(str) == 1 || k >= 26 ||
				(fig[k++] = ft_check_figure(str)) == 0)
				return (0);
			read(fd, &c, 1);
			i = 0;
		}
		else if (i == 20 || (i < 20 && c == '\0'))
			return (0);
	}
	while (k < 27)
		fig[k++] = '\0';
	if (i != 0)
		return (0);
	return (fig);
}
