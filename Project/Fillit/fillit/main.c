/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtelega <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/14 12:59:41 by dtelega           #+#    #+#             */
/*   Updated: 2017/01/06 10:40:51 by dtelega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"
#include <stdio.h>

int		main(int ac, char **av)
{
	char	*figures;
	int		size;
	int		hesh;
	char	*finish_map;

	if (ac != 2)
	{
		write(1, "usage: ./fillit [input_file]\n", 29);
		return (1);
	}
	if ((figures = ft_read(av[1], 0, 0)) == 0)
	{
		write(1, "error\n", 6);
		return (1);
	}
	hesh = ft_strlen(figures) * 4;
	size = 0;
	while (ft_sqr(size) < hesh)
		size++;
	finish_map = NULL;
	finish_map = ft_creator(finish_map, figures, size);
	printf("figures : %s\n", figures);
	printf("*********\n\n%s\n\n***********\n",finish_map);
	write(1, "complete!\n", 10);
	return (0);
}
