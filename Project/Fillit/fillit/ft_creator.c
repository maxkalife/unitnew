/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_creator.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtelega <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/23 14:50:23 by dtelega           #+#    #+#             */
/*   Updated: 2017/01/08 17:41:19 by mkaliber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

char	*ft_creator(char *finish_map, char *figures, int size)
{
	int		size_s;
	int		n;
	char	letter;

	printf("OPEN CREATOR\n");
	size_s = size * size + size;
	if (!(finish_map = (char *)malloc((size_s + 1) * sizeof(*finish_map))))
		return (0);
	finish_map[size_s--] = '\0';
	while (size_s >= 0)
		finish_map[size_s--] = '.';
	finish_map = ft_clear(finish_map, size);
	finish_map = ft_all_put(finish_map, figures, size, 0, 0);
	printf("------close_creator----\n----finish_map:---\n%s\n", finish_map);
	return (finish_map);
}

char	*ft_all_put(char *finish_map, char *figures,
					int size, int index_fig, int start)
{
	int		temp;

	temp = size;
	if (figures[index_fig] && temp == size)
	{
		finish_map = ft_put_fig(finish_map, figures, index_fig,
								&size, start);
//		start = 0;
//		index_fig++;
	}
	return (finish_map);
}

char	*ft_clear(char *finish_map, int size)
{
	int		i;
	int		count_n;

	printf("OPEN CLEAR\n"); //********
	i = 0;
	count_n = 0;
	while (finish_map[i] != '\0')
	{
		if (count_n == size)
		{
			finish_map[i] = '\n';
			count_n = -1;
		}
		i++;
		count_n++;
	}
	printf("CLOSE CLEAR\n");
	return (finish_map);
}
