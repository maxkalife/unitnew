/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_put_fig.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtelega <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/26 15:11:47 by dtelega           #+#    #+#             */
/*   Updated: 2017/01/07 15:24:40 by dtelega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

char	*ft_clear_letter(char *finish_map, char letter);

char	*ft_put_fig(char *finish_map, char *figures, int index_fig,
					int *size, char letter, int k)
{
	int     i;
	char    *fig;
	int     tmp;
	int		tmp2;

	i = 0;
	tmp = 0;
	tmp2 = k;

	fig = ft_database((int)(figures[index_fig]) - 17 - '0');

	while (fig[i] && finish_map[i + k])
	{
		if (fig[i] == '#' && finish_map[i + k] == '.')
		{
			finish_map[i + k] = letter;
			printf("\n\n%s\n\n", finish_map);
		}
		else if (fig[i] != '#' && fig[i] != '\0')
			k += *size - ((int)fig[i] - '0') - 1;
		else if (finish_map[i + k] != '.' && finish_map[i + k] != '\0' && fig[i] == '#')
		{
			finish_map = ft_clear_letter(finish_map, letter);
			i = -1;
			tmp++;
			k = tmp;
		}
		i++;
	}
	if (fig[i] != '\0' && index_fig > 0)
	{
		finish_map = ft_clear_letter(finish_map, letter);
		finish_map = ft_clear_letter(finish_map, --letter);
		finish_map = ft_all_put(finish_map, figures, *size, --index_fig, ++tmp2);
		return (finish_map);
	}


/*		finish_map = ft_clear_letter(finish_map, letter);
		finish_map = ft_clear_letter(finish_map, --letter);
		finish_map = ft_all_put(finish_map, figures,
		size, --index_fig, ++tmp2);*/
	if (fig[i] != '\0')
	{
		printf("--------------go size+1\n");
		*size += 1;
		finish_map = ft_creator(finish_map, figures, *size);
	}
	finish_map = ft_all_put(finish_map, figures, *size, index_fig + 1, 0);
	printf("FINISH ft_put_fig\n////\n%s/////\n", finish_map);
	return (finish_map);
}

char	*ft_clear_letter(char *finish_map, char letter)
{
	int		i;

	i = 0;
	while (finish_map[i])
	{
		if (finish_map[i] == letter)
			finish_map[i] = '.';
		i++;
	}
	return (finish_map);
}
