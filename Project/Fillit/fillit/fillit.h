/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtelega <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/16 13:29:09 by dtelega           #+#    #+#             */
/*   Updated: 2017/01/08 18:41:54 by mkaliber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLIT_H
# define FILLIT_H

# include <fcntl.h>
# include <sys/types.h>
# include <sys/uio.h>
# include <unistd.h>
# include <stdlib.h>

# include <stdio.h> //**************************

char		*ft_read(char *field, int i, int k);
int			ft_strlen(const char *s);
int			ft_check_fail(char *str);
char		ft_check_figure(char *str);
char		ft_check_figure1(char *str, int i);
char		*ft_creator(char *finish_map, char *figures, int size);
char		*ft_clear(char *finish_map, int size);
int			ft_sqr(int nb);
char		*ft_database(int i);
char		*ft_all_put(char *finish_map, char *figures, int size,
						int index_fig, int start);
char		*ft_put_fig(char *finish_map, char *figures, int index_fig,
						int *size, int k);
char		*ft_clear_letter(char *finish_map, char letter);

#endif
