/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   typers_sc_l.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkaliber <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/07 16:12:50 by mkaliber          #+#    #+#             */
/*   Updated: 2017/02/09 14:58:46 by mkaliber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "printf.h"


void    *typer_s(va_list ap)
{
	char *p;

	p = va_arg(ap, char *);
	return (p);
}


void    *typer_S_l(va_list ap)
{
  return (va_arg(ap,  wchar_t *));
}

void    *typer_c(va_list ap)
{
	char *p;

	if (!(p = (char *)malloc(2 * sizeof(p))))
		return (0);
	p[0] = va_arg(ap, int);
	p[1] = '\0';
	return (p);
}

void    *typer_C_l(va_list ap)
{
	 char *p;

	if (!(p = (char *)malloc(2 * sizeof(p))))
		return (0);
	p[0] = va_arg(ap, wint_t);
	p[1] = '\0';
	return (p);
}