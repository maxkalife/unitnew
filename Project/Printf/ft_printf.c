/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkaliber <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/06 16:00:20 by mkaliber          #+#    #+#             */
/*   Updated: 2017/03/24 19:23:03 by mkaliber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "printf.h"
#include <stdio.h>
//char    *typer(va_list ap);


int    ft_printf(char *format, ...)
{
	va_list ap;
	int n;
    int i;
    void *(*f[36])(va_list ap);

    f[0] = typer_s;
    f[1] = typer_S_l;
    f[2] = typer_c;
    f[3] = typer_C_l;
    f[4] = typer_p;
    f[5] = typer_di;
    f[6] = typer_di_h;
    f[7] = typer_di_hh;
    f[8] = typer_di_l;
    f[9] = typer_di_ll;
    f[10] = typer_di_j;
    f[11] = typer_di_z;
    f[12] = typer_o;
    f[13] = typer_O;
    f[14] = typer_oO_h;
    f[15] = typer_oO_hh;
    f[16] = typer_oO_l;
    f[17] = typer_oO_ll;
    f[18] = typer_oO_j;
    f[19] = typer_oO_z;
    f[20] = typer_u;
    f[21] = typer_U;
    f[22] = typer_uU_h;
    f[23] = typer_uU_hh;
    f[24] = typer_uU_l;
    f[25] = typer_uU_ll;
    f[26] = typer_uU_j;
    f[27] = typer_uU_z;
    f[28] = typer_x;
    f[29] = typer_X;
    f[30] = typer_xX_h;
    f[31] = typer_xX_hh;
    f[32] = typer_xX_l;
    f[33] = typer_xX_ll;
    f[34] = typer_xX_j;
    f[35] = typer_xX_z;

    ind = 0;
    i = 0;
    home1.iter = 0;
	va_start(ap, format);
    gi = 0;
	while (i < ft_lenap(format))
    {
        n = check(format);
        home1.str_gen = ft_strnew(0);
        home1.str_gen = ft_strjoin(home1.str_gen, f[n](ap));
        home1.str_text_f = ft_strnew(0);
        home1.str_text_f = check_text_f(format);
        home1.str_text_s = ft_strnew(0);
        home1.str_text_s = check_text_s(format);
        glen = ft_strlen(home1.str_gen);
        home1.str_flag = ft_strnew(0);
        iter_flag(format);

        home1.str_gen = ft_strjoin(home1.str_flag, home1.str_gen);

        home1.str_gen = ft_strjoin(home1.str_text_f, home1.str_gen);
        home1.str_gen = ft_strjoin(home1.str_gen, home1.str_text_s);
       /* while (home1.str_gen[n])
        {
            write(1, &home1.str_gen[n], 1);
            gi++;
            n++;
        }*/
        ft_putstr(home1.str_gen);
        free(home1.str_text_f);
        free(home1.str_text_s);
        free(home1.flag_list);
        free(home1.str_gen);
        free(home1.str_flag);
        home1.g = 0;
        home1.gsp = 0;
        home1.iter = 0;
        home1.minus = 0;
        home1.plus = 0;
        home1.null = 0;
        i++;
    }
	va_end (ap);
	return (gi);
}
