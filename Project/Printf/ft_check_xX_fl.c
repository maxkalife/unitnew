/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_xX_fl.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkaliber <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/09 10:12:13 by mkaliber          #+#    #+#             */
/*   Updated: 2017/02/09 15:18:39 by mkaliber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "printf.h"

int		check_xX_hh_ll(char *str, int tmp)
{
    int h;
    int l;

    h = 0;
    l = 0;
    while (str[tmp] == 'h')
    {
        tmp++;
        h++;
    }
    if (h == 1)
        return (30);
    if (h == 2)
        return (31);
    while (str[tmp] == 'l')
    {
        tmp++;
        l++;
    }
    if (l == 1)
        return (32);
    if (l == 2)
        return (33);
    return (1);
}

int		check_xX_j(char *str, int tmp)
{
	while (str[tmp] != '%' && str[tmp])
	{
		if (str[tmp] == 'j')
		{
			return (34);
		}
		tmp++;
	}
	return (1);
}

int		check_xX_z(char *str, int tmp)
{
	while (str[tmp] != '%' && str[tmp])
	{
		if (str[tmp] == 'z')
		{
			return (35);
		}
		tmp++;
	}
	return (1);
}