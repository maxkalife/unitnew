/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   typers_oO_hh_ll.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkaliber <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/09 15:07:20 by mkaliber          #+#    #+#             */
/*   Updated: 2017/02/09 16:23:04 by mkaliber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "printf.h"


void    *typer_oO_h(va_list ap)
{
    char *p;

    p = ft_itoa_base((unsigned short int)va_arg(ap, unsigned long long), 8);
    return (p);
}

void    *typer_oO_hh(va_list ap)
{
    char *p;

    p = ft_itoa_base((unsigned char)va_arg(ap, unsigned long long), 8);
    return (p);
}

void    *typer_oO_l(va_list ap)
{
    char *p;

    p = ft_itoa_base(va_arg(ap, unsigned long int), 8);
    return (p);
}

void    *typer_oO_ll(va_list ap)
{
    char *p;

    p = ft_itoa_base(va_arg(ap, unsigned long long int), 8);
    return (p);
}