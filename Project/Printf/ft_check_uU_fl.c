/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_uU_fl.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkaliber <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/09 10:12:13 by mkaliber          #+#    #+#             */
/*   Updated: 2017/02/09 15:18:39 by mkaliber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "printf.h"

int		check_uU_hh_ll(char *str, int tmp)
{
    int h;
    int l;

    h = 0;
    l = 0;
    while (str[tmp] == 'h')
    {
        tmp++;
        h++;
    }
    if (h == 1)
        return (22);
    if (h == 2)
        return (23);
    while (str[tmp] == 'l')
    {
        tmp++;
        l++;
    }
    if (l == 1)
        return (24);
    if (l == 2)
        return (25);
    return (1);
}

int		check_uU_j(char *str, int tmp)
{
	while (str[tmp] != '%' && str[tmp])
	{
		if (str[tmp] == 'j')
		{
			return (26);
		}
		tmp++;
	}
	return (1);
}

int		check_uU_z(char *str, int tmp)
{
	while (str[tmp] != '%' && str[tmp])
	{
		if (str[tmp] == 'z')
		{
			return (27);
		}
		tmp++;
	}
	return (1);
}