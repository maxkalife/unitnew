/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   iterflag.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkaliber <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/16 14:14:33 by mkaliber          #+#    #+#             */
/*   Updated: 2017/03/16 14:14:35 by mkaliber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "printf.h"

void    iter_flag(char *form)
{
    home1.iter = 0;
    flag_list(form);
    if (check_flag(home1.flag_list) == 'd' || check_flag(home1.flag_list) == 'o' || check_flag(home1.flag_list) == 'u')
        ft_gen_minus();
    if (check_flag(home1.flag_list) == 'd')
    {
        space();
        plus();
    }
    while (home1.flag_list[home1.iter] != '%' && home1.flag_list[home1.iter])
    {
        if (home1.flag_list[home1.iter] == '0')
        {
            home1.null = 1;
        }
        if (home1.flag_list[home1.iter] == '.')
        {

            check_accuracy();
        }
        if (home1.flag_list[home1.iter - 1] == '-' && home1.flag_list[home1.iter] > '0' && home1.flag_list[home1.iter] <= '9')
        {
            check_width_def();
        }
        if (home1.flag_list[home1.iter] <= '9' && home1.flag_list[home1.iter - 1] != '.' && home1.flag_list[home1.iter - 1] != '-')
        {

            check_width_wdef();
        }
        home1.iter++;
    }
    if (check_flag(home1.flag_list) == 'd' || check_flag(home1.flag_list) == 'o' || check_flag(home1.flag_list) == 'u')
    {
        if (home1.gsp == 1)
        {
            home1.str_flag = ft_strjoin(" ", home1.str_flag);
            space();
        }
        if (home1.plus == 1 && home1.minus != 1)
        {
            if (home1.gsp == 1)
                home1.str_flag[0] = '+';
            else
                home1.str_flag = ft_strjoin("+", home1.str_flag);
        }
        if (home1.str_flag[0] != '-' && home1.minus == 1)
            home1.str_flag = ft_strjoin("-", home1.str_flag);
    }
}

void    flag_list(char *form)
{
    int n;
    int g;

    n = 0;
    while (form[gil] != '%' && form[gil])
    {
        gil++;
    }
    gil++;
    g = gil;
    while (form[gil] != '%' && form[gil])
    {
        gil++;
        n++;
    }
    home1.flag_list = ft_strnew(n);
    n = 0;
    while (form[g] != '%' && form[g])
    {
        home1.flag_list[n] = form[g];
        g++;
        n++;
    }
    home1.flag_list[n] = '\0';
}

void    ft_gen_minus()
{
    glen = ft_strlen(home1.str_gen);
    if (home1.str_gen[0] == '-')
    {
        home1.str_gen = ft_strsub(home1.str_gen, 1, glen - 1);
        home1.minus = 1;
        glen = ft_strlen(home1.str_gen);
    }
}

int     save_n_acc_gen()
{
    int n;

    n = 0;
        if (home1.flag_list[home1.iter] == '.' && home1.flag_list[home1.iter + 1] >= '0' && home1.flag_list[home1.iter + 1] <= '9' &&
            home1.flag_list[home1.iter])
        {
            home1.iter++;
            n = n + (home1.flag_list[home1.iter] - '0');
            home1.iter++;
            while (home1.flag_list[home1.iter] >= '0' && home1.flag_list[home1.iter] <= '9' && home1.flag_list[home1.iter])
            {
                n = n * 10 + (home1.flag_list[home1.iter] - '0');
                home1.iter++;
            }
            home1.iter--;
            return (n);
        }
    return (n);
}

int     save_n_acc()
{
    int n;
    int i;

    i = 0;
    n = 0;
    while (home1.flag_list[i])
    {
        if (home1.flag_list[i] == '.' && home1.flag_list[i + 1] >= '0' && home1.flag_list[i + 1] <= '9' &&
            home1.flag_list[i])
        {
            i++;
            n = n + (home1.flag_list[i] - '0');
            i++;
            while (home1.flag_list[i] >= '0' && home1.flag_list[i] <= '9' && home1.flag_list[i])
            {
                n = n * 10 + (home1.flag_list[i] - '0');
                i++;
            }

            return (n);
        }
        i++;
    }
    return (n);
}

int     save_n_width_gen()
{
    int n;

    n = 0;
    if (home1.flag_list[home1.iter - 1] != '.' && home1.flag_list[home1.iter] > '0' && home1.flag_list[home1.iter] <= '9')
    {
        n = n + (home1.flag_list[home1.iter] - '0');
        home1.iter++;
        while (home1.flag_list[home1.iter] >= '0' && home1.flag_list[home1.iter] <= '9' && home1.flag_list[home1.iter])
        {
            n = n * 10 + (home1.flag_list[home1.iter] - '0');
            home1.iter++;
        }
        home1.iter--;
        return (n);

    }
    return (n);
}

int     save_n_width()
{
    int n;
    int i;

    i = 0;
    n = 0;
    while (home1.flag_list[i])
    {
        if (home1.flag_list[i] != '.' && home1.flag_list[i + 1] > '0' && home1.flag_list[i + 1] <= '9')
        {
            i++;
            n = n + (home1.flag_list[i] - '0');
            i++;
            while (home1.flag_list[i] >= '0' && home1.flag_list[i] <= '9' && home1.flag_list[i])
            {
                n = n * 10 + (home1.flag_list[i] - '0');
                i++;
            }
            return (n);
        }
        i++;
    }
    return (n);
}

char    inc_null_d()
{
    if (home1.null == 1 && save_n_acc() == 0 && gw != 1)
        return ('0');
    return (' ');
}

void    space()
{
    int i;

    i = 0;
    while (home1.flag_list[i])
    {
        if (home1.flag_list[i] == ' ')
            home1.gsp = 1;
        i++;
    }
}

void    plus()
{
    int i;

    i = 0;
    while (home1.flag_list[i])
    {
        if (home1.flag_list[i] == '+')
            home1.plus = 1;
        i++;
    }
}