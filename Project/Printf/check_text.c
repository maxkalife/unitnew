/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_text.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkaliber <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/14 16:30:50 by mkaliber          #+#    #+#             */
/*   Updated: 2017/03/14 16:30:54 by mkaliber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "printf.h"

void    *check_text_f(char *str)
{
    int n;
    char *s;

    n = ind;
    while (str[n] != '%' && str[n])
        n++;
    s = ft_strnew(n);
    n = 0;
    while (str[ind] != '%' && str[ind])
    {
        s[n] = str[ind];
        n++;
        ind++;
    }
    return (s);
}

void    *check_text_s(char *str)
{
    int n;
    char *s;
    n = ind;
    ind++;
    while (str[n] != '%' && str[n])
    {
        n++;
    }
    s = ft_strnew(n);
    n = 0;
    while (str[ind] != '%' && str[ind])
    {
        s[n] = str[ind];
        n++;
        ind++;
    }
    return (s);
}
