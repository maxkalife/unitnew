/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   modef.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkaliber <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/15 17:59:47 by mkaliber          #+#    #+#             */
/*   Updated: 2017/03/15 17:59:49 by mkaliber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "printf.h"

void    check_space()
{
    int i;

    i = 0;
    if (home1.flag_list[home1.iter] == ' ' && home1.minus != 1 && home1.gsp != 1)
    {
        home1.str_flag = ft_strjoin(home1.str_flag, " ");
        space();
    }
    i++;
}

void    check_accuracy()
{
    int n;
    int i;
    char *res;

    i = 0;
    n = i;

    if (home1.flag_list[home1.iter] == '.')
    {

        gw = 1;
        n = save_n_acc_gen() - glen;
        if (n > 0)
        {
            res = ft_strnew(n);
            while (n-- > 0)
                res[i++] = '0';
            res[i] = '\0';
            if (home1.minus == 1)
                home1.str_flag = ft_strjoin("-", home1.str_flag);
            home1.str_flag = ft_strjoin(home1.str_flag, res);
        }
        else if (n < 0)
        {
            n = save_n_acc();
            home1.str_gen = ft_strsub(home1.str_gen, 0, n);
        }
    }
}


void    check_width_def()
{
    int n;
    int i;
    int r;
    int f;
    char *res;

    i = 0;
    n = i;
    r = glen;
    f = 0;
    if ((save_n_acc() - glen) >= 0)
        r = save_n_acc();
    if (home1.flag_list[home1.iter - 1] == '-')
    {
        n = save_n_width_gen() - r - home1.gsp - home1.minus;
        if (n > 0)
        {
            f = 0;
            res = ft_strnew(n);
            while (n-- > 0)
                res[f++] = ' ';
            home1.str_text_s = ft_strjoin(res, home1.str_text_s);

        }
    }
}

void    check_width_wdef()
{
    int n;
    int i;
    int r;
    int f;
    char *res;

    i = 0;
    n = i;
    f = 0;
    r = glen;
    if ((save_n_acc() - glen) >= 0)
        r = save_n_acc();
    if (home1.flag_list[home1.iter] > '0' && home1.flag_list[home1.iter] <= '9' && home1.flag_list[home1.iter - 1] != '-' && home1.flag_list[home1.iter])
    {
        n = save_n_width_gen() - r - home1.gsp - home1.minus;
        if (n > 0)
        {
            f = 0;
            res = ft_strnew(n);
            while (n-- > 0)
                res[f++] = inc_null_d();
            if (inc_null_d() == '0')
                home1.str_flag = ft_strjoin(home1.str_flag, res);
            else
                home1.str_text_f = ft_strjoin(home1.str_text_f, res);

        }
    }
}