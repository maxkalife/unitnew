#include <stdio.h>
#include "ft_list.h"

t_list *ft_create_elem(void *data);
void ft_list_push_back(t_list **begin_list, void *data);
void ft_list_push_front(t_list **begin_list, void *data);
int ft_list_size(t_list *begin_list);

void print(t_list *list)
{
	while (list)
	{
		printf("%s -> ", list->data);
		list = list->next;
	}
}
int		main(void)
{
	t_list *list;
	
	list = NULL;
	printf("\n");
	printf("nb = %d\n", ft_list_size(list));
	return (0);
}
