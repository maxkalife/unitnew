/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_size.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: khrechen <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/09 21:34:27 by khrechen          #+#    #+#             */
/*   Updated: 2016/11/09 22:03:41 by khrechen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"

int		ft_list_size(t_list *begin_list)
{
	int		nb;
	t_list	*temp;

	nb = 0;
	temp = begin_list;
	while (temp)
	{
		nb++;
		temp = temp->next;
	}
	return (nb);
}
