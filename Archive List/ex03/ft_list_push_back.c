/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_push_back.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: khrechen <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/09 18:12:46 by khrechen          #+#    #+#             */
/*   Updated: 2016/11/13 13:56:03 by khrechen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"

void	ft_list_push_back(t_list **begin_list, void *data)
{
	t_list *end;
	t_list *header;

	end = ft_create_elem(data);
	if (*begin_list)
	{
		header = *begin_list;
		while (header->next)
			header = header->next;
		header->next = end;
	}
	else
		*begin_list = end;
}
