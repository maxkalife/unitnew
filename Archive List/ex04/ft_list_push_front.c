/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_push_front.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: khrechen <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/09 21:31:30 by khrechen          #+#    #+#             */
/*   Updated: 2016/11/09 21:32:27 by khrechen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_list.h"

void	ft_list_push_front(t_list **begin_list, void *data)
{
	t_list *head;

	head = ft_create_elem(data);
	if (*begin_list != NULL)
	{
		head->next = *begin_list;
		*begin_list = head;
	}
	else
		*begin_list = head;
}
