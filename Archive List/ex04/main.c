#include <stdio.h>
#include "ft_list.h"

t_list *ft_create_elem(void *data);
void ft_list_push_back(t_list **begin_list, void *data);
void ft_list_push_front(t_list **begin_list, void *data);
t_list *ft_list_last(t_list *begin_list);

void print(t_list *list)
{
	while (list)
	{
		printf("%s -> ", list->data);
		list = list->next;
	}
}
int		main(void)
{
	t_list *list;
	
	list = NULL;
	//list = ft_create_elem("mam");
	//ft_list_push_back(&list, "jj");
	//ft_list_push_front(&list, "aaa");
	print(list);
	printf("\n");
	print(ft_list_last(list));
	return (0);
}
