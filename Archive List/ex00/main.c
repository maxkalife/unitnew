#include <stdio.h>
#include "ft_list.h"

t_list	*ft_create_elem(void *data);

void print(t_list *list)
{
	while (list)
	{
		printf("%s -> ", list->data);
		list = list->next;
	}
}
int		main(void)
{
	char *data[] = {"mama", "mula", "ramy", 0};

	int i = 0;
	while (data[i] != 0)
	{
		print(ft_create_elem(data[i]));
		i++;
	}
	return (0);
}
