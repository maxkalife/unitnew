/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_push_params.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: khrechen <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/09 22:35:45 by khrechen          #+#    #+#             */
/*   Updated: 2016/11/09 23:00:49 by khrechen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"

t_list	*ft_list_push_params(int ac, char **av)
{
	t_list	*begin;
	t_list	*head;
	int		i;

	i = 1;
	begin = ft_create_elem(av[0]);
	while (i < ac)
	{
		head = ft_create_elem(av[i]);
		head->next = begin;
		begin = head;
		i++;
	}
	return (begin);
}
