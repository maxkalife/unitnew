#include <stdio.h>
#include "ft_list.h"

t_list *ft_create_elem(void *data);
void ft_list_push_back(t_list **begin_list, void *data);
void ft_list_push_front(t_list **begin_list, void *data);
t_list *ft_list_last(t_list *begin_list);
t_list *ft_list_push_params(int ac, char **av);

void print(t_list *list)
{
	while (list)
	{
		printf("%s -> ", list->data);
		list = list->next;
	}
}
int		main(void)
{
	t_list *list;
	char *a[] = {"aaa", "ooo", "ccc", "ddd"};
	//list = NULL;
	list = ft_list_push_params(4, a);
	
	print(list);
	printf("\n");
	return (0);
}
