#include <stdio.h>
#include "ft_list.h"


void print(t_list *list)
{
	while (list)
	{
		printf("%s\n", list->data);
		printf("%p\n", list->next);
		list = list->next;
	}
}
int		main(void)
{
	t_list *list;
	
	list = NULL;
	list = ft_create_elem("mam");
	print(list);
	printf("\n");
	ft_list_push_front(&list, "pap");
	print(list);
	return (0);
}
