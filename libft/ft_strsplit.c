/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkaliber <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/12 11:09:31 by mkaliber          #+#    #+#             */
/*   Updated: 2016/12/21 19:21:08 by mkaliber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdio.h>

static int		bust(char *str, char c, int i)
{
	while (str[i] == c && str[i] != '\0')
		i++;
	return (i);
}

static int		bust_nc(char *str, char c, int i)
{
	int l;
	int n;

	n = i;
	l = 0;
	while (str[n] != c && str[n] != '\0')
	{
		n++;
		l++;
	}
	return (l);
}

static int		count_words(char *str, char c)
{
	int i;
	int count;
	int s;

	count = 0;
	i = 0;
	while (str[i] != '\0')
	{
		s = 0;
		while (str[i] == c && str[i] != '\0')
			i++;
		while (str[i] != c && str[i] != '\0')
		{
			s++;
			i++;
		}
		if (s > 0)
			count++;
	}
	return (count);
}

char			**ft_strsplit(char const *s, char c)
{
	char	**str;
	int		rows;
	size_t	len;
	size_t	i;

	if (!s)
		return (NULL);
	rows = count_words((char *)s, c);
	len = 0;
	i = 0;
	if (!(str = (char **)malloc((rows + 1) * sizeof(*str))))
		return (NULL);
	str[rows] = NULL;
	rows = 0;
	while (str[rows] != NULL)
	{
		i = bust((char *)s, c, (int)i);
		len = bust_nc((char *)s, c, (int)i);
		str[rows] = ft_strsub(s, i, len);
		i = i + len;
		rows++;
	}
	return (str);
}

int main()
{
	char **str;

	str = ft_strsplit("salut****", '*');
	printf("%s", str[0]);
	return (0);
}
