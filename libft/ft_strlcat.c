/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkaliber <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/04 17:31:44 by mkaliber          #+#    #+#             */
/*   Updated: 2016/12/08 17:20:00 by mkaliber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t		ft_strlcat(char *dst, const char *src, size_t size)
{
	size_t i;
	size_t temps;
	size_t tempd;

	i = 0;
	temps = ft_strlen(src);
	tempd = ft_strlen(dst);
	if (size < tempd)
		return (temps + size);
	while (temps > 0 && i < (size - tempd - 1))
	{
		dst[tempd + i] = src[i];
		i++;
	}
	dst[tempd + i] = '\0';
	return (tempd + temps);
}
